import React, { Component } from 'react';
import './Home.css'
import PlayerStatus from './PlayerStatus';

class Home extends Component {
    constructor(props){
        super(props)
        this.state = {
            board: Array(9).fill(null),
            player: null,
            winner: null
        }
    }
    handleClick(index){
       if (this.state.player && !this.state.winner) {
            let newBoard = this.state.board
            if (this.state.board[index] === null) {
              newBoard[index] = this.state.player
              this.setState({
                board: newBoard,
                player: this.state.player === "X" ? "O" : "X"
              })
              this.checkWinner()
            }
          }
       }
        checkWinner() {
            let winLines =
              [
                ["0", "1", '2'],
                ["3", "4", '5'],
                ["6", "7", '8'],
                ["0", "3", '6'],
                ["1", "4", '7'],
                ["2", "5", '8'],
                ["0", "4", '8'],
                ["2", "4", '6'],
              ]
            this.checkMatch(winLines)
          }
        
          checkMatch(winLines) {
            for (let index = 0; index < winLines.length; index++) {
              const [a, b, c] = winLines[index];
              let winBoard = this.state.board
              if (winBoard[a] && winBoard[a] === winBoard[b] && winBoard[a] === winBoard[c]) {
                this.setState({
                  winner: this.state.player
                })
              }
            }
          }
          setPlayer(player){
              this.setState({ player })
          }
          reset() {
            this.setState({
              player: null,
              winner: null,
              board: Array(9).fill(null)
            })
          }

    renderBoxes(){
        return this.state.board.map(
            (box, index) =>
              <div className="box" key={index}
                onClick={() => this.handleClick(index)}>
                {box} </div>
          )
        }
render(){
    return(
        <div className="container">
        <h1>Tic Tac Toe Game</h1>
        <PlayerStatus player={this.state.player}
        setPlayer={(e) => { this.setPlayer(e) }}
        winner={this.state.winner}/>
        <div className="board">
            {this.renderBoxes()}
        </div>
        <button  onClick={() => this.reset()}> Reset</button >
        </div>
    )
    }
}
export default Home;