import React, { Component } from 'react';
import ChoosePlayer from './ChoosePlayer';

class PlayerStatus extends Component {
    handleSetPlayer(e){
        this.props.setPlayer(e)
    }
    renderPlayer(){
        if(this.props.winner){
            return ( <h2>Winner is {this.props.winner}</h2>)
        } 
        else {
            return this.props.player ? 
            <h2>Now player is {this.props.player}</h2> :
            <ChoosePlayer player={(e) => this.handleSetPlayer(e)}/>
        }
    }
    render(){
        return (<span>{this.renderPlayer()}</span>)
    }
}

export default PlayerStatus;